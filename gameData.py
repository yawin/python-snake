import screen

class Snake:
    def __init__(self, pantalla):
        self.x = pantalla.width//2
        self.y = pantalla.height//2
        self.size = 3
        self.dir_x, self.dir_y = (1,0)

    def update(self, pantalla):
        self.x = (self.x + self.dir_x) % pantalla.width
        self.y = (self.y + self.dir_y) % pantalla.height

        dataVal = pantalla.getValue(self.x, self.y)

        if dataVal < 0:
            self.size = self.size + 1
        elif dataVal > 0:
            self.size = -1
            return

        pantalla.setValue(self.x, self.y, self.size)

    def changeDir(self, dir_x, dir_y):
        self.dir_x = dir_x
        self.dir_y = dir_y
