class Screen:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.data = [[0 for x in range(width)] for y in range(height)]

    def getValue(self, x, y):
        return self.data[y][x]

    def setValue(self, x, y, value):
        self.data[y][x] = value

    def update(self):
        for i in range(self.height):
            for j in range(self.width):
                if self.data[i][j] > 0:
                    self.data[i][j] = self.data[i][j]-1

    def getDraw(self):
        drawing = ""
        for i in range(self.height):
            for j in range(self.width):
                pixel = " "
                if(self.data[i][j] > 0):
                    pixel = '*'
                elif(self.data[i][j] < 0):
                    pixel = "#"
                drawing = drawing + pixel
            drawing = drawing + "\n"
        return drawing

    def draw(self):
        print ("\x1B[1;1H")
        print (self.getDraw())
