import screen
import gameData
import time, termios, sys

def newGame():
    pantalla = screen.Screen(32, 10)
    snake = gameData.Snake(pantalla)
    return [pantalla, snake]


game = newGame()

def pressed():
  """
  Returns a pressed key (Supposedly non-blocking)
  """

  def isData():
    return select.select([sys.stdin], [], [], 0.01)==([sys.stdin], [], [])

  c=""
  old_settings=termios.tcgetattr(sys.stdin)
  try:
      tty.setcbreak(sys.stdin.fileno())
      if isData():
          c=sys.stdin.read(1)
  finally:
      termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_settings)
      return c

def inpt():
    global game
    key = pressed()

    if key == "w":
        game[1].changeDir(0,-1)
    elif key == "s":
        game[1].changeDir(0,1)
    elif key == "a":
        game[1].changeDir(-1,0)
    elif key == "s":
        game[1].changeDir(1,0)

def update():
    global game
    game[0].update()
    game[1].update(game[0])

def draw():
    global game
    game[0].draw()

def loop():
    while 1:
        inpt()
        update()
        draw()
        time.sleep(1/30)#30


if __name__ == "__main__":
    loop();
